package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.Model.Product;


public interface ProductRepository extends CrudRepository<Product, Integer>{

	public Product searchByTitle(String title);
}
