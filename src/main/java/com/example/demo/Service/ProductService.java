package com.example.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Product;
import com.example.demo.repository.ProductRepository;


@Service
public class ProductService {
	
	@Autowired
	private ProductRepository repo;
	
	public ProductService() {}

	public ProductService(ProductRepository repo) {
		super();
		this.repo = repo;
	}
	
	public void saveProduct(Product product)
	{
		repo.save(product); 
	}
	
	public Iterable<Product> showAllProducts()
	{
		return repo.findAll();
	}
		
	public Product findByTitle(String title) {
		return repo.searchByTitle(title);
	}
}
