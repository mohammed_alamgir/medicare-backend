package com.example.demo.Controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.Model.Product;
import com.example.demo.Model.User;
import com.example.demo.Service.ProductService;
import com.example.demo.Service.UserService;

@org.springframework.web.bind.annotation.RestController
public class RestController {
	@Autowired
	private UserService service;
	@Autowired
	private ProductService ps;
	
	@PostMapping("/save-user")
	@Transactional
	@CrossOrigin
	public String registerUser(@RequestBody User user)
	{
		service.saveMyUser(user);
		
		return "Hello " + user.getFirstname()+" your registration is successful!";
	}
	
	@PostMapping("/save-product")
	@Transactional
	@CrossOrigin
	public String saveProduct(@RequestBody Product product)
	{
		ps.saveProduct(product);
		
		return "Hello " + product.getTitle()+" got saved!";
	}
	
	@GetMapping("/all-users")
	@CrossOrigin
	public Iterable<User> showAllUsers() {
		return service.showAllUsers();
	}
	
	@GetMapping("/all-products")
	@CrossOrigin
	public Iterable<Product> showAllProducts() {
		return ps.showAllProducts();
	}
	
	@GetMapping("/delete/{username}")
	@Transactional
	public Iterable<User> deleteUser(@PathVariable String username){
		return service.deleteUserByUsername(username);
	}
	
	@GetMapping("/search/{username}")
	public User searchUser(@PathVariable String username) {
		return service.findByUsername(username);
	}
		
	@GetMapping("/search-product/{title}")
	@CrossOrigin
	public Product searchProduct(@PathVariable String title) {
		return ps.findByTitle(title);
	}
}
